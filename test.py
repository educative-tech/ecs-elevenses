#!/usr/bin/env python3

import handler

class FakeContext:
    pass

ctx = FakeContext()
event = {}

res = handler.refresh(event, ctx)
print(res)
