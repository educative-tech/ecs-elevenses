# ECS Elevenses

### Because even a server needs a tea break

When you run the lambda function `refresh`, all ecs services with the tag "refresh" set to "enable" (and with at least two active tasks) will have the oldest task stopped.


## Installing

Run `serverless deploy -r us-east-2`

or use whatever region you prefer instead of us-east-2.

Then add the tag refresh=enable to all the services you want managed in ECS. The default schedule time is at midnight UTC
