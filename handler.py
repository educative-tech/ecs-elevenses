##  MIT Licence
##
##  Copyright (c) 2022 Educative Tech
##
##  Permission is hereby granted, free of charge, to any person obtaining a copy
##  of this software and associated documentation files (the "Software"), to deal
##  in the Software without restriction, including without limitation the rights
##  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
##  copies of the Software, and to permit persons to whom the Software is
##  furnished to do so, subject to the following conditions:
##
##  The above copyright notice and this permission notice shall be included in all
##  copies or substantial portions of the Software.
##
##  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
##  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
##  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
##  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
##  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
##  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
##  SOFTWARE.

import boto3
from pprint import pprint

def refresh(event, context):
    ecs = boto3.client("ecs")
    services = []
    for cluster in ecs.list_clusters()['clusterArns']:
        arns = ecs.list_services(cluster=cluster)['serviceArns']
        for service in ecs.describe_services(cluster=cluster, services=arns, include=['TAGS'])['services']:
            try:
                if not any(t.get('key', '') == 'refresh' and t.get('value', '') == 'enable' for t in service.get('tags', [])):
                    continue
                tasks_arns = ecs.list_tasks(cluster=cluster, serviceName='canvas-lms-share-web').get('taskArns', [])
                if not tasks_arns:
                    continue
                tasks = [task for task in ecs.describe_tasks(cluster=cluster, tasks=tasks_arns).get('tasks', []) if task['lastStatus'] =='RUNNING']
                if len(tasks) > 1:
                    task = min(tasks, key=lambda o: o['startedAt'])
                    ecs.stop_task(cluster=cluster, task=task['taskArn'], reason='Good task. Take a break.')
            except Exception as e:
                pprint(e)



